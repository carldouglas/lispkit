#!/bin/bash

CMD=./lispkit
COMPILER_SRC=compiler.txt.ascii
COMPILER_BIN=compiler.ascii

for f in `ls tests/execute-??`
do
  echo "Running function test $f ..."
  $CMD $f $f-args
  if [ $? -ne 0 ]; then exit -1; fi;
done

for f in `ls tests/compile-??`
do
  echo "Running compiler test $f ..."
  if [ ! -f compiler.ascii ]; then echo "no compiler" && exit -1; fi;
  $CMD $COMPILER_BIN $f
  if [ $? -ne 0 ]; then exit -1; fi;
done

echo "Bootstrapping ..."
$CMD $COMPILER_BIN $COMPILER_SRC

